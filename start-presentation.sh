#!/bin/sh

docker run -it --rm -p 9999:9999 \
    -v $(pwd):/home/jovyan/work \
    -e NB_UID=$(id -u) -e NB_GID=$(id -g) \
    jupyter/scipy-notebook:1fbaef522f17 jupyter nbconvert /home/jovyan/work/0-presentation.ipynb --to slides --post serve --ServePostProcessor.port=9999 --ServePostProcessor.ip='*'

